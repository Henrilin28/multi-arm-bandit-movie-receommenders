# Bandit for Recommendation system MVP

### Project structure

```
services
    ├── spark
    │   ├── Dockerfile
    │   ├── conf
    │   └── data
    │  
    └── web
        ├── Dockerfile
        └── project
            ├── __init__.py
            ├── client
            │   ├── static
            │   │   ├── css
            │   │   └── js
            │   └── templates   
            │  
            ├── data
            │   ├── cosSim.dat
            │   ├── jaccardSim.dat
            │   └── movieInfo.pkl
            ├── manage.py
            ├── requirements.txt
            ├── server
            │   ├── __init__.py
            │   ├── config.py
            │   └── main
            │       ├── __init__.py
            │       ├── bandits.py
            │       ├── models
            │       │   ├── ALSRecommender.py
            │       │   ├── IBRModel.py
            │       │   └── __init__.py
            │       ├── tasks.py
            │       └── views.py
            └── spark-warehouse
```

### Introduction
It is started from a code sample in multi-arm bandit, then I think it would be cool to build a online multi-arm bandit system MVP with off-line machine learning algorithms.
The goal here is to apply multi-arm bandit algorithm to measure the quality of a recommendation system. The quality of a recommender system is often measured by A/B testing experiments. However, A/B testing has traditionally been used to measure metrics in static features. what brought me to explore multi-armed bandits as an alternative to A/B testing in the scope of recommender systems.

This project implemented the `explore and explicit` multi-arm bandit algorithm with some movie recommenders. These recommenders are  based on the similarity of movies genre, overview description and users ratings. The models are mostly being trained and saved in the local disk except for the ALS spark recommender. You can explore the MVP by running docker on your machine.

#### Services perspectives

This project can be divided into three services:

- Spark: This service currently has one master and one worker node.
- Flask: This the web service. It controls the view and model of the web service.
- Redis: The redis service is job queue for spark only.

#### Recommenders system perspectives

The recommendations systems includes both Collaborative Filtering and Content Based Filtering which are considered as arms as follws:

- JaccardMovieRec(Ratings): Jaccard Similarity based on user's ratings
- CosineMovieRec(Genere): Cosine Similarity based on user's ratings
- CorrelationMovieRec(Ratings): Jaccard Similarity based on user's ratings
- ALSrecommendation(Spark): MLlib recommender in collaborative filtering


### Example

![](https://thumbs.gfycat.com/HideousFrailEstuarinecrocodile-small.gif)

### Quick Start


Download docker engine and Spin up the containers:

```sh
$ docker-compose up
```

Open your browser to http://localhost:5009

