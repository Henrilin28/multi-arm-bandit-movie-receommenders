

var recommendation = {
    rewarded: false,
    init: function(){
        this.movies();
        $('#reward-btn').hide();
        $('.step-2').hide();
        $('.step-3').hide();
        $('#result').hide();

        $("#rec").submit(function(e) {
            $('#reward-btn').prop('disabled',false);
            $('#reward-btn').text('Submit movie preference');
            $('#reward-btn').hide();
            recommendation.rewarded = false;
            e.preventDefault();
            recommendation.submit();
            global.resize();
        });

        movies.forEach(function(movie){
            $("#movies").append('<option value="'+movie+'">'+movie+'</option>');
        });

        $(".chosen-select").chosen({no_results_text: "Oops, nothing found!"});
        $('.movie:not(.active)').on('click', function(){
            if (!recommendation.rewarded){
                $(".movie.active").removeClass('active');
                $(this).addClass('active');
                var item_rank = $(this).attr('data-id');
                $('#current-selection').val(item_rank);
                $('#reward-btn').show();
                $('.step-3').show();
                global.resize();
            }
        });

        $('#reward-btn').on('click', function(e){
            e.preventDefault();
            recommendation.rewarded = true;
            $("#reward-btn").text("Thanks!");
            $("#reward-btn").prop('disabled',true);
            var item_num = $('#current-selection').val(),
            id = $('#u-id').val();
            $('#u-id').val('');
            $('#current-selection').val('10');
            try{
                int_num = parseInt(item_num,10);
                if (int_num > 9){
                    throw new Exception();
                } else if (int_num < 0) {
                    throw new Exception();
                }
                var payload = {
                    id: id,
                    item: item_num
                };

                $.ajax({
                    type: "POST",
                    url: "/update",
                    async: false,
                    // timeout: 3000,
                    data: JSON.stringify(payload),
                    contentType: "application/json",
                    success: function(d) {
                        var arm = d.arm,
                            armName = d.arm_name,
                            reward = d.reward;
                        if (isNaN(arm)){
                            arm = parseInt(arm,10);
                        }
                        arm = arm + 1;
                        var text = ["<h3>Result:</h3>",
                                    "<hr>","The recommender used was <code>" + armName.toString() + "</code>","<br>",
                                    "The recommender is arm <code>" + arm + "</code> of the bandit","<br>",
                                    "Your selection produced a reward of <code>" + reward.toString() + "</code>"
                                    ].join('');
                        $('#result').html(text);
                        $('#result').show();

                        global.resize();
                    }

                });
            } catch (err){
                var text = ["<h3>Result:</h3>",
                            "<hr>Sorry an error occurred please retry your search"];
                $('#result').html(text);
                $('#result').show();
                global.resize();
            }

        });
    },
    // make this image link
    movies: function(){
        for (var i=0; i < 5; i++){
            $('.col-1').append('<li class="movie" data-id="'+i+'">'+
                                '<img class="poster">'+
                               '<span class="name"></span></li>');
        }
        for (var i=5; i < 10; i++){
            $('.col-2').append('<li class="movie" data-id="'+i+'">'+
                                '<img class="poster">'+
                                '<span class="name"></span></li>');
        }
    },
    submit: function(){

        var chosen_movie = $("#movies").val();
        var payload = {
            movies: chosen_movie
        };

        $.ajax({
            type: "POST",
            url: "/recommendation",
            data: JSON.stringify(payload),
            contentType: "application/json",
            success: function(d) {
                recommendation.reset();
                if (d.result.length){
                    $('.results p').addClass('show');
                }
                for (var i=0; i < 5; i++){
                    var $this_movie = $('.col-1 .movie:nth-child('+(i+1)+')');
                    $this_movie.find('.name').html(d.result[i].movie);
                    $this_movie.find('.poster').attr("src",d.result[i].url);
                    $this_movie.addClass('show');
                }
                for (var i=5; i < 10; i++){
                    var $this_movie = $('.col-2 .movie:nth-child('+(i-4)+')');
                    $this_movie.find('.name').html(d.result[i].movie);
                    $this_movie.find('.poster').attr("src",d.result[i].url);
                    $this_movie.addClass('show');
                }
                $('#u-id').val(d.uid);
                $('.step-2').show();
                $('#result').hide().children('code').text('');
                $(".movie").removeClass('active');
                setTimeout(function(){
                    global.resize();
                }, 300);
            }
        });
        global.resize();
        return false;
    },
    reset: function(){
        $('.results p').removeClass('show');
        $('.movie').removeClass('show');
         $(".movie").removeClass('active');
        $('#result').hide().children('code').text('');
        global.resize();
    }
};

var global = {
    init: function(){
        global.resize();


        $(window).on('resize', function(){
            global.resize();
        });
    },
    resize: function(){
        $('.jumbotron').css('min-height', $('body').height());
    }
};


function refreshPage(){
    window.location.reload();
}



$(document).ready(function(){
    recommendation.init();
    global.init();
});
