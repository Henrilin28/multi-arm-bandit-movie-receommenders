# --------------------------------------------
# bandit + movie recommendation ban
#
#
# author: Chuan-heng(Henry) Lin
# --------------------------------------------


import numpy as np
import math


def ind_max(x):
    m = max(x)
    return x.index(m)


class UCB:
    def __init__(self, n):
        """
        Initialize Bandit Algo
        Parameters
        ------
        views: number of views
        converted: number of clicks / views
        n: numbers of arm(machine)

        """
        self.views = [0] * n
        self.converted = [0.] * n
        self.n = n

    def pull(self):
        """
        choose an arm for testing.(Decide which bandit machine to pull)

        Returns
        ------
        arm: int, the function will return the arm number based on
                  exploit and explore method
        """
        n_arms = len(self.n)
        for arm in range(n_arms):
            if self.views[arm] == 0:
                return arm

        ucb_values = [0.0 for arm in range(n_arms)]
        total_counts = sum(self.views)
        for arm in range(n_arms):
            bonus = math.sqrt((2 * math.log(total_counts)) / float(self.views[arm]))
            ucb_values[arm] = self.converted[arm] + bonus
        return ind_max(ucb_values)

    def generate_reward(self, arm, reward):
        """
         Update an arm with reward value
         targets: click = 1; no click = 0

        Parameters
        ------
        arm: int, selected recommender
        reward: float,  it is caculated by 1. - (item_num / 9.)
        """

        self.views[arm] = self.views[arm] + 1
        n = self.views[arm]

        value = self.converted[arm]
        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.values[chosen_arm] = new_value


class EpsilonGreedy:
    def __init__(self, n, decay=100):
        """
        Initialize Bandit Algo
        Parameters
        ------
        views: number of views
        converted: number of clicks / views
        n: numbers of arm(machine)

        """

        self.views = [0] * n
        self.converted = [0.] * n
        self.decay = decay
        self.n = n

    def get_epsilon(self):
        """
        calculate epsilon(the probability to explore at each time step.)
        produces a variable value based on the sum of the user feedback
        Returns
        ------
        epsilon
        """
        total = np.sum(self.views)
        return float(self.decay) / (total + float(self.decay))

    def pull(self):
        """
        choose an arm for testing.(Decide which bandit machine to pull)

        Returns
        ------
        arm: int, the function will return the arm number based on
                  exploit and explore method
        """
        epsilon = self.get_epsilon()
        if np.random.random() > epsilon:
            # Exploit (use best arm for testing)
            return np.argmax(self.converted)
        else:
            # Explore (test random arm )
            return np.random.randint(self.n)

    def generate_reward(self, arm, reward):
        """
         Update an arm with reward value
         targets: click = 1; no click = 0

        Parameters
        ------
        arm: int, selected recommender
        reward: float,  it is caculated by 1. - (item_num / 9.)
        """
        self.views[arm] = self.views[arm] + 1
        n = self.views[arm]
        value = self.converted[arm]

        new_value = ((n - 1) / float(n)) * value + (1 / float(n)) * reward
        self.converted[arm] = new_value
