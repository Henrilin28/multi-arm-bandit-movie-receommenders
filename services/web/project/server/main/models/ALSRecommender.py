# --------------------------------------------
# bandit + movie recommendation ban
#
#
# author: Chuan-heng(Henry) Lin
# --------------------------------------------


import os
import pandas as pd
import time

# spark imports
from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import col, lower
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS


# the wheel is built on top of this post
# https://spark.apache.org/docs/2.3.1/ml-collaborative-filtering.html
class ALSRecommender:
    """
    AlS spark implementation
    """
    def __init__(self, spark_s, movies_path, ratings_path):
        self.spark = spark_s
        self.sc = spark_s.sparkContext
        self.movies = self.load_file(movies_path) \
                          .select(['movieId', 'title'])
        self.ratings = self.load_file(ratings_path) \
                           .select(['userId', 'movieId', 'rating'])
        self.model = ALS(
                        userCol='userId',
                        itemCol='movieId',
                        ratingCol='rating',
                        coldStartStrategy="drop")
        self.data_path = os.path.join('/usr/src/app', 'data')

    def load_file(self, file_path):
        """
        load csv file as spark dataframe
        """
        return self.spark.read.load(file_path, format='csv',
                                    header=True, inferSchema=True)

    def get_movieId(self, fav_movie_list):
        """
        return all movieId(s) of the inputs from front-end

        Parameters
        ----------
        fav_movie_list: list, list of selected movies

        Return
        ------
        movieId_list: list of movieId(s)
        """
        movieId_list = []
        for movie in fav_movie_list:
            movieIds = self.movies \
                .filter(self.movies.title.like('%{}%'.format(movie))) \
                .select('movieId') \
                .rdd \
                .map(lambda r: r[0]) \
                .collect()
            movieId_list.extend(movieIds)
        return list(set(movieId_list))

    def set_model_params(self, maxIter, reg, rank):
        """
        set model params for pyspark.ml.recommendation.ALS
        Parameters
        ----------
        maxIter: integer, learning iterations
        reg: float, regularization parameter
        ranks: float, number of latent factors
        """
        self.model = self.model \
                         .setRegParam(reg)\
                         .setMaxIter(maxIter) \
                         .setRank(rank)

    def _inference(self, model, fav_movieIds, n_recommendations):
        """
        return top n movie recommendations based on user's input movie
        Parameters
        ----------
        model: spark ALS model
        fav_movie: str, name of user input movie
        n_recommendations: int, top n recommendations
        Return
        ------
        list of top n similar movie recommendations
        """
        # create a userId
        userId = self.ratings.agg({"userId": "max"}).collect()[0][0] + 1
        # get movieIds of favorite movies
        # append new user with his/her ratings into data
        user_rdd = self.sc.parallelize(
            [(userId, movieId, 4.0) for movieId in fav_movieIds])
        # transform to user rows
        user_rows = user_rdd.map(
            lambda x: Row(
                userId=int(x[0]),
                movieId=int(x[1]),
                rating=float(x[2])
            )
        )
        # transform rows to spark DF
        userDF = self.spark.createDataFrame(user_rows) \
            .select(self.ratings.columns)
        # reattach to ratings dataframe
        self.ratings = self.ratings.union(userDF)

        model = model.fit(self.ratings)
        # filter movies
        other_movieIds = self.movies \
                             .filter(~col('movieId').isin(fav_movieIds)) \
                             .select(['movieId']) \
                             .rdd.map(lambda r: r[0]) \
                             .collect()

        # create inference rdd
        inferenceRDD = self.sc.parallelize(
            [(userId, movieId) for movieId in other_movieIds]
            ).map(
                lambda x: Row(
                    userId=int(x[0]),
                    movieId=int(x[1]),
                )
            )

        # restructure to inference result
        inferDF = self.spark.createDataFrame(inferenceRDD) \
            .select(['userId', 'movieId'])
        # make inference
        return model.transform(inferDF) \
            .select(['movieId', 'prediction']) \
            .orderBy('prediction', ascending=False) \
            .rdd.map(lambda r: (r[0], r[1])) \
            .take(n_recommendations)

    def make_recommendations(self, fav_movieTitles, n_recommendations):
        """
        make top n movie recommendations
        Parameters
        ----------
        fav_movieTitles: list, name of user input movie
        n_recommendations: int, top n recommendations

        Return
        ------
        movie_titles: list, top n movie recommendations
        score: list, top n movie score
        """

        # make inference and get raw recommendations
        print('Recommendation system start to make inference ...')
        t0 = time.time()
        fav_movieIds = self.get_movieId(fav_movieTitles)
        raw_recommends = \
            self._inference(self.model, fav_movieIds, n_recommendations)
        movieIds = [r[0] for r in raw_recommends]
        scores = [r[1] for r in raw_recommends]

        return movieIds


