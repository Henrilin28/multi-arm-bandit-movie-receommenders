# --------------------------------------------
# bandit + movie recommendation ban
#
#
# author: Chuan-heng(Henry) Lin
# --------------------------------------------

import os
import pandas as pd
import numpy as np


class IBRModel:
    """
        content(item)-based recommendations
    """

    # Inititialize the movie Dictionary and cosine similairty matrix
    def __init__(self, name):
        self.data_path = os.path.join('/usr/src/app', 'data')
        self.name = name
        self.movies = pd.read_pickle(os.path.join(self.data_path, 'movieInfo.pkl'))

    def similarity(self, name):
        """
        load similariy into memory

        Return
        ------
        movieId_list: list of movieId(s)

        """

        if self.name == 'CosineMovieRec(Genere)':
            return np.load(os.path.join(self.data_path, 'cosSim.dat'))
        if self.name == 'JaccardMovieRec(Ratings)':
            return np.load(os.path.join(self.data_path, 'jaccardSim.dat'))
        if self.name == 'CorrelationMovieRec(Ratings)':
            return np.load(os.path.join(self.data_path, 'corSim.dat'))

    def predict(self, fav_movieTitles, topK=10):
        """
        Generate recommendations from the cosine similarity of movie genere.
        Parameters
        ----------
        fav_movieTitles: list, name of user input movie
        topK: int, top n recommendations

        Returns
        n_recommendations: list, list of recommendations
        ----------

        """
        indices = pd.Series(self.movies.index, index=self.movies['title'])
        titles = self.movies['title']
        similarity = self.similarity(self.name)
        sim_scores = np.sum(similarity[indices[fav_movieTitles]], 0)
        sorted_scores = sorted(list(enumerate(sim_scores)),
                               key=lambda x: x[1], reverse=True)
        topK_scores = sorted_scores[:topK]
        n_recommendations = [i[0] for i in topK_scores]

        return n_recommendations

    def get_info(self, n_recommendations):
        """
        Generate recommendations from the cosine similarity of movie genere.
        Parameters
        ----------
        n_recommendations: int, top n recommendations
        Returns
        ----------
        movie_titles: list, top n movie recommendations
        n_recommendations: int, top n recommendations
        imageURL: list, top n movie poster image url
        """
        titles = self.movies['title']\
                     .iloc[n_recommendations]\
                     .tolist()

        imageURL = self.movies['imageURL']\
                       .iloc[n_recommendations]\
                       .tolist()

        return titles, imageURL
