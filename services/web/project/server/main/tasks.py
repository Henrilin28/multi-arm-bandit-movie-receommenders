# services/web/server/main/views.py
# --------------------------------------------
# bandit + movie recommendation ban
#
#
# author: Chuan-heng(Henry) Lin
# --------------------------------------------


try:

    # from pyspark import SparkContext, SparkConf
    # from operator import add
    # spark imports
    import os
    import argparse
    import time
    import gc

    # spark imports
    from pyspark.sql import SparkSession, Row
    from server.main.models.ALSRecommender import ALSRecommender


except Exception as e:
    print('error{}'.format(e))


def create_task(fav_movies_list):
    # spark config
    data_path = os.path.join('/tmp/data/', 'ml-latest-small')
    # initial spark
    spark = SparkSession \
        .builder \
        .appName("movie recommender") \
        .getOrCreate()

    # initial recommender system
    recommender = ALSRecommender(
        spark,
        os.path.join(data_path, 'movies.csv'),
        os.path.join(data_path, 'ratings.csv'))
    # set params
    recommender.set_model_params(5, 0.01, 20)
    # make recommendations
    recommendations = recommender.make_recommendations(fav_movies_list, 10)
    # stop
    spark.stop()
    return recommendations
