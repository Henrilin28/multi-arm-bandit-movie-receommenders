# services/web/server/main/views.py
# --------------------------------------------
# bandit + movie recommendation ban
#
#
# author: Chuan-heng(Henry) Lin
# --------------------------------------------


import redis
import pickle
from rq import Queue, push_connection, pop_connection
from flask import current_app, render_template, url_for, Blueprint, jsonify, request, abort, Response
from uuid import uuid4
import numpy as np
import pandas as pd
from server.main.bandits import EpsilonGreedy, UCB
from server.main.models.IBRModel import IBRModel
from server.main.tasks import create_task

arms = ["JaccardMovieRec(Ratings)",
        "CosineMovieRec(Genere)",
        "CorrelationMovieRec(Ratings)",
        "ALSrecommendation(Spark)"
        ]
multi_bandit = EpsilonGreedy((len(arms)))  # UCB((len(arms))
ids = {}
main_blueprint = Blueprint('main', __name__,)


# user input will triger the bandit machine and pick one of the algorithm to perform recommendation
@main_blueprint.route('/recommendation', methods=['POST'])
def recommendation():
    q = Queue()
    arm = multi_bandit.pull()
    arm_name = arms[arm]
    u_id = str(uuid4())

    # initiate model based on the arm
    # get movie recommendations based on the cosine similarity score of movie genres
    model = IBRModel(arm_name)

    # ALS implementation
    if arm_name == "ALSrecommendation(Spark)":
        task = q.enqueue(create_task, request.json['movies'])
        while(task.is_finished == False):
            task = q.fetch_job(task.get_id())

        topKindices = task.result
    else:
        topKindices = model.predict(request.json['movies'])

    titles, imageURL = model.get_info(topKindices)
    # form requests
    result = []
    for movie, url in zip(titles, imageURL):
        result.append({"movie": movie,
                       "url": url})
    # keep track of experiment variables
    ids[u_id] = {
        'arm': arm,
        'arm_name': arm_name
    }
    return jsonify({'result': result, 'uid': u_id})


# index page to show the result of selected recommender result
@main_blueprint.route('/', methods=['GET', 'POST'])
def index():

    # static files
    css_url = url_for('static', filename='css/main.css')
    jquery_url = url_for('static', filename='js/jquery-1.10.2.min.js')
    movies_url = url_for('static', filename='js/movies.js')
    highlight_url = url_for('static', filename='js/code.js')
    js_url = url_for('static', filename='js/main.js')
    return render_template('index.html', css_url=css_url,
                            jquery_url=jquery_url,
                            movies_url=movies_url,
                            js_url=js_url,
                            highlight_url=highlight_url)


# when user click submit button -> the frontend will send ajax response to the endpoint
# IT WILL UPDATE THE REWARD FOR THE SELECTED RECOMMENDER
@main_blueprint.route('/update', methods=['POST'])
def update_path():
    try:
        u_id, item_num = request.json['id'], int(request.json['item'])
        # divided by (n-1)
        reward = 1. - (item_num / 9.)
        if reward > 1. or reward < 0.:
            raise Exception()
        reward = reward ** 2
        arm_data = ids.pop(u_id)
        arm = arm_data['arm']
        multi_bandit.generate_reward(arm, reward)
        arm_data['reward'] = reward
        arm_data['arm_name'] = arms[arm]

        return jsonify(arm_data)
    except Exception:

        return jsonify({'bad data': "Sorry, please try it again"})


# this page will show the status of each recommenders bandit results
@main_blueprint.route('/state', methods=['GET'])
def get_results():
    state = {}
    state['epsilon'] = multi_bandit.get_epsilon()
    state['arms'] = []
    for i in range(len(arms)):
        arm_data = {}
        arm_data['name'] = arms[i]
        arm_data['number'] = i
        arm_data['count'] = multi_bandit.views[i]
        arm_data['value'] = multi_bandit.converted[i]
        state['arms'].append(arm_data)
    return render_template('state.html', state=state)

# push connection to redis
@main_blueprint.before_request
def push_rq_connection():
    push_connection(redis.from_url(current_app.config['REDIS_URL']))


# pop connection from redis
@main_blueprint.teardown_request
def pop_rq_connection(exception=None):
    pop_connection()
